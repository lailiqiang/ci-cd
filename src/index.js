import $ from "jquery";
import _ from "lodash";

import "./index.css";

setTimeout(() => {
  $("#app").html("jQuery Awesome <br /> Lodash Awesome");
}, 3333);

console.log("index =>", _.shuffle([1, 2, 3, 4, 5, 6]).join(','));
