const path = require("path");
const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const ExtractCssChunks = require("extract-css-chunks-webpack-plugin");
const DashboardPlugin = require("webpack-dashboard/plugin");
const BundleAnalyzerPlugin = require("webpack-bundle-analyzer").BundleAnalyzerPlugin;

const fileName = "[name].[chunkhash:8]";

module.exports = {
  // externals: {
  //   jquery: "jQuery",
  //   lodash: "_",
  // },
  mode: "development",
  entry: {
    main: "./src/index.js",
    app: "./src/app.js",
  },
  output: {
    filename: fileName + ".js",
    path: path.resolve(__dirname, "dist"),
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        include: path.resolve(__dirname, "src"),
        use: "babel-loader",
      },
      {
        test: /\.css$/,
        include: path.resolve(__dirname, "src"),
        use: [
          {
            loader: ExtractCssChunks.loader,
            options: {
              hot: true, //如果你想要HMR  - 我们尝试自动注入热重新加载但是如果它不起作用，将它添加到配置
              modules: true, //如果你使用cssModules，这可以提供帮助。
              reloadAll: true, //当绝望开始时 - 这是一个暴力HMR旗帜
            },
          },
          "css-loader",
        ],
      },
    ],
  },
  optimization: {
    splitChunks: {
      chunks: "all", //async异步代码分割 initial同步代码分割 all同步异步分割都开启
      minSize: 30000, //字节 引入的文件大于30kb才进行分割
      //maxSize: 50000,         //50kb，尝试将大于50kb的文件拆分成n个50kb的文件
      minChunks: 2, //模块至少使用次数
      maxAsyncRequests: 5, //同时加载的模块数量最多是5个，只分割出同时引入的前5个文件
      maxInitialRequests: 3, //首页加载的时候引入的文件最多3个
      automaticNameDelimiter: "~", //缓存组和生成文件名称之间的连接符
      name: true,
      cacheGroups: {
        //缓存组，将所有加载模块放在缓存里面一起分割打包
        vendors: {
          //自定义打包模块
          test: /[\\/]node_modules[\\/]/,
          priority: -10, //优先级，先打包到哪个组里面，值越大，优先级越高
          filename: "vendors.js",
        },
        default: {
          //默认打包模块
          priority: -20,
          reuseExistingChunk: true, //模块嵌套引入时，判断是否复用已经被打包的模块
          filename: "common.js",
        },
      },
    },
  },
  plugins: [
    new ExtractCssChunks({
      // 类似于webpackOptions.output中相同选项的选项
      //两个选项都是可选
      filename: fileName + ".css",
      chunkFilename: "[id].css",
      orderWarning: true, //禁用删除有关冲突顺序的警告进口
    }),
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      filename: "index.html",
      template: path.resolve(__dirname, "index.html"),
      inject: "body",
    }),
    new DashboardPlugin(),
    new BundleAnalyzerPlugin(),
  ],
  devServer: {
    contentBase: path.join(__dirname, "dist"),
    compress: true,
    port: 9000,
    host: "0.0.0.0",
    open: true,
    https: true,
  },
};
